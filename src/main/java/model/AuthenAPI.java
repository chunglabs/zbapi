package model;

public class AuthenAPI {
    private final String baseUri = "http://auth.ns3083172.ip-145-239-7.eu.zerobank.net";
    private final String basePath = "connect/token";
    public String getBaseUri() {
        return baseUri;
    }

    public String getBasePath() {
        return basePath;
    }
    private String username;
    private String password;
    private String grant_type;
    private String scope;
    private String client_id;
    private String client_secret;
    private String CodeVerifier;

    public AuthenAPI(String username, String password) {
        this.username = username;
        this.password = password;
        this.grant_type = "password";
        this.scope = "openid offline_access zb_api";
        this.client_id = "ro.client";
        this.client_secret = "secret";
        this.CodeVerifier = "2";
    }

    public AuthenAPI(String username, String password, String grant_type, String scope, String client_id, String client_secret, String codeVerifier) {
        this.username = username;
        this.password = password;
        this.grant_type = grant_type;
        this.scope = scope;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.CodeVerifier = codeVerifier;
    }

    public String getRequestBody(){
        String body =
                "username="+username+"&password="+password+
                        "&grant_type=password&scope=openid%20offline_access%20zb_api&client_id=ro.client&client_secret=secret&CodeVerifier=3";
        return body;
    }

    public String getToken(){
        return "Basic cm8uY2xpZW50OnNlY3JldA==";
    }
}
