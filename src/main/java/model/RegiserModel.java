package model;

public class RegiserModel {
   private String password;
   private String confirmPassword;
   private String phonePrefix;
   private Boolean IsAgent;
   private String countryCode;
   private String phone;
   private String invitationCode;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public Boolean getAgent() {
        return IsAgent;
    }

    public void setAgent(Boolean agent) {
        IsAgent = agent;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public RegiserModel(String password, String confirmPassword, String phonePrefix,
                        Boolean isAgent, String countryCode, String phone, String invitationCode) {
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.phonePrefix = phonePrefix;
        IsAgent = isAgent;
        this.countryCode = countryCode;
        this.phone = phone;
        this.invitationCode = invitationCode;
    }

    public void toJsonCode(){

    }
}
