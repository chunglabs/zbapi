package common;

public enum  API {
    //authen API
    AUTHEN_URL("http://auth.ns3083172.ip-145-239-7.eu.zerobank.net"),
    AUTHEN("connect/token"),

    //My account
    API_URL("http://api.ns3083172.ip-145-239-7.eu.zerobank.net"),
    GET_NOTIFICATION("api/v1/user/get-notifications"),
    GET_FREE_TOKEN("api/v1/user/request-free-token"),
    GET_MY_PROFILE("api/v1/user/get_my_profile"),
    REGISTER("api/v1/user/register");

    private String url;
    API(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }
}
