package StepLibraries;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class ResponseStatus {
    @Step
    public void verifytatusCode(int status){
        int statusCode = SerenityRest.lastResponse().statusCode();
        Assert.assertEquals(status,statusCode);
    }
}
