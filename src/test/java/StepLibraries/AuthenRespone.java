package StepLibraries;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class AuthenRespone {
    @Step
    public void verifyErrorMessage(String errorMessage){
        String message = SerenityRest.lastResponse().getBody().jsonPath().getString("error");
        Assert.assertEquals(errorMessage,message);
    }

    @Step
    public void verifyErrorDescrption(String description){
        String message = SerenityRest.lastResponse().getBody().jsonPath().getString("error_description");
        Assert.assertEquals(description,message);
    }
}
