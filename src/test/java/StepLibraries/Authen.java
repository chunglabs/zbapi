package StepLibraries;

import common.API;
import model.AuthenAPI;
import common.ShareData;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class Authen {;
    AuthenAPI authenAPI;
    @Step
    public void authenRequest(String username, String password){
        authenAPI = new AuthenAPI(username,password);
        SerenityRest.given()
                .baseUri(API.AUTHEN_URL.getUrl())
                .basePath(API.AUTHEN.getUrl())
                .header("Content-Type","application/x-www-form-urlencoded")
                .header("Authorization",authenAPI.getToken())
                .contentType("application/x-www-form-urlencoded")
                .body(authenAPI.getRequestBody())
                .when().post();
        String token = SerenityRest.lastResponse().getBody().jsonPath().getString("access_token");
        ShareData.authenToken = token;
    }

}
