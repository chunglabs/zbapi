package StepLibraries;

import common.API;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class GetNotification {
    @Step
    public void getNotificationRequest(String token){
        SerenityRest.given()
                .baseUri(API.API_URL.getUrl())
                .basePath(API.GET_NOTIFICATION.getUrl())
                .header("Content-Type","application/json")
                .header("Authorization","Bearer "+token)
                .when().get();
    }
}
