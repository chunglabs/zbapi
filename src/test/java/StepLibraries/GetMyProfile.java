package StepLibraries;

import common.API;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class GetMyProfile {
    @Step("Send get my profile request")
    public void getMyProFileRequest(String token){
        SerenityRest.given()
                .baseUri(API.API_URL.getUrl())
                .basePath(API.GET_MY_PROFILE.getUrl())
                .header("Content-Type","application/json")
                .header("Authorization","Bearer "+token)
                .when().get();
    }
}
