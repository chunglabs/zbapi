package stepdefinitions;

import StepLibraries.Authen;
import StepLibraries.GetMyProfile;
import StepLibraries.GetNotification;
import StepLibraries.ResponseStatus;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import common.ShareData;
import net.thucydides.core.annotations.Steps;

public class MyAcountSteps {
    @Steps
    Authen authen;

    @Steps
    GetMyProfile getMyProfile;

    @Steps
    ResponseStatus responseStatus;

    @Steps
    GetNotification getNotification;

    @Given("^Tan created a authen request with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void tan_created_a_authen_request_with_and(String username, String password) {
        authen.authenRequest(username,password);
    }

    @When("^He send Get my Profile request$")
    public void he_send_Get_my_Profile_request() {
        getMyProfile.getMyProFileRequest(ShareData.authenToken);
    }

    @Then("^He should see his profile and status code is (\\d+)$")
    public void he_should_see_his_profile_and_status_code_is(int statusCode) {
        responseStatus.verifytatusCode(statusCode);
    }
    @When("^He send Get notification request$")
    public void he_send_Get_notification_request() {
        getNotification.getNotificationRequest(ShareData.authenToken);
    }

    @Then("^He should see notification and status code is (\\d+)$")
    public void he_should_see_notification_and_status_code_is(int statusCode) {
        responseStatus.verifytatusCode(statusCode);
    }
}
