package stepdefinitions;

import StepLibraries.Authen;
import StepLibraries.AuthenRespone;
import StepLibraries.ResponseStatus;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AuthenSteps {
    @Steps
    Authen authen;
    @Steps
    ResponseStatus responseStatus;
    @Steps
    AuthenRespone authenRespone;

    @When("^Jankos login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void jankos_login_with_and(String username, String password) {
        authen.authenRequest(username,password);
    }

    @Then("^He should see status code (\\d+) and a token$")
    public void he_should_see_status_code_and_a_token(int status) {
            responseStatus.verifytatusCode(status);
    }
    @Then("^He should see status code (\\d+) and error \"([^\"]*)\"$")
    public void he_should_see_status_code_and_error(int status, String errorMessage) {
        responseStatus.verifytatusCode(status);
        authenRespone.verifyErrorMessage(errorMessage);
    }

    @Then("^He should see status code (\\d+) and description \"([^\"]*)\"$")
    public void he_should_see_status_code_and_description(int status, String description) {
        responseStatus.verifytatusCode(status);
        authenRespone.verifyErrorDescrption(description);
    }

}
