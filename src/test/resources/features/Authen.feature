Feature: Authen
  Scenario: login successfully
    When Jankos login with "84984298305" and "123456789aA"
    Then He should see status code 200 and a token

  Scenario: login with blank username
    When Jankos login with "" and "123456789aA"
    Then He should see status code 400 and error "invalid_grant"

  Scenario: login with wrong username
    When Jankos login with "09334343434345" and "123456789aA"
    Then He should see status code 400 and description "Incorrect username"

  Scenario: login with blank password
    When Jankos login with "84984298305" and ""
    Then He should see status code 400 and error "invalid_grant"

  Scenario: login with wrong password
    When Jankos login with "84984298305" and "2313132Aa"
    Then He should see status code 400 and description "Incorrect password"

